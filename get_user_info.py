import csv

def inputName():
	f = open('users.csv', 'w', newline='')
	wr = csv.writer(f)
	print("이름을 입력하세요.(이름(Firstname Lastname))")
	name = input()
	wr.writerow([name])
	f.close()
	return

def inputEmail():
	f = open('users.csv', 'w', newline='')
	wr = csv.writer(f)
	print("메일주소를 입력하세요.")
	email = input()
	wr.writerow([email])
	f.close()
	return

def inputPhoneNumber():
	f = open('users.csv', 'w', newline='')
	wr = csv.writer(f)
	print("전화번호를 입력하세요.")
	pn = input()
	wr.writerow([pn])
	f.close()
	return
